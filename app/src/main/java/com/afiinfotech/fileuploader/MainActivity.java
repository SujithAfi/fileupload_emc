package com.afiinfotech.fileuploader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.fileuploader.Model.PatientData;
import com.afiinfotech.fileuploader.Model.SelectedFileModel;
import com.afiinfotech.fileuploader.Utilis.Utilities;
import com.afiinfotech.fileuploader.adapter.MainPageFragmentAdapter;
import com.afiinfotech.fileuploader.dialogs.UploadDialog;
import com.afiinfotech.fileuploader.fragments.FileFragment;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements UploadDialog.DialogActionListner, MainPageFragmentAdapter.DialogActionListner {

    public boolean showcompleteFlag = true;
    boolean doubleBackToExitPressedOnce = false;
    private ViewPager viewPager;
    private SmartTabLayout tabLayout;
    private PatientData patientData = null;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private CoordinatorLayout container;
    private FileUploadApplication fileUploadApplication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewpgerMain);
        tabLayout = (SmartTabLayout) findViewById(R.id.tabLayout);
        container = (CoordinatorLayout) findViewById(R.id.cl_container);

        fileUploadApplication = (FileUploadApplication) this.getApplication();

        showcompleteFlag = true;

        getSupportActionBar().setTitle("File uploader");
        getSupportActionBar().setSubtitle("User :" + Utilities.getSharedPreferences(this).getString("user_name", ""));
        String a = Utilities.getSharedPreferences(this).getString("user_name", "");
        getSupportActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState != null) {
            MainPageFragmentAdapter adapter = new MainPageFragmentAdapter(getSupportFragmentManager(), this);
            int tabsize = savedInstanceState.getInt("tabSize");
            adapter.setTabSize(tabsize);
            viewPager.setAdapter(adapter);
        } else {
            viewPager.setAdapter(new MainPageFragmentAdapter(getSupportFragmentManager(), this));
        }

        tabLayout.setViewPager(viewPager);

        findViewById(R.id.btnLoad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilities.hideKeyboard(MainActivity.this);
                if (((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString().trim().length() > 0)
                    loadData(((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString());
                else {
                    ((EditText) findViewById(R.id.edtTxtPatientId)).setError("Enter Patient file id");
                    findViewById(R.id.edtTxtPatientId).requestFocus();
                    Utilities.showSnackBar("Enter Patient file id", MainActivity.this);
                }

            }
        });


        findViewById(R.id.addAttachmentButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainPageFragmentAdapter) viewPager.getAdapter()).addTab();
                viewPager.setCurrentItem(((MainPageFragmentAdapter) viewPager.getAdapter()).getTabSize() - 1, true);
                tabLayout.setViewPager(viewPager);
            }
        });

        findViewById(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcompleteFlag = false;
                removeSelectedAttachment();
            }
        });

        findViewById(R.id.btnUpload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainPageFragmentAdapter) viewPager.getAdapter()).Update();

                Utilities.hideKeyboard(MainActivity.this);
                showcompleteFlag = true;
                if (patientData == null) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Validation error")
                            .setMessage("Patient not selected,please load patient data by providing patient id")
                            .setPositiveButton("ok", null)
                            .setCancelable(false)
                            .create()
                            .show();
                } else {

                    if (viewPager.getAdapter() != null) {
                        List<SelectedFileModel> selectedFileModels = ((MainPageFragmentAdapter) viewPager.getAdapter()).getFileModelList();
                        if (selectedFileModels.isEmpty()) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("No attachment selecled")
                                    .setMessage("Please select an attachment")
                                    .setPositiveButton("OK", null)
                                    .create()
                                    .show();
                        } else {

                            uploadFiles(selectedFileModels);
                        }
                    }

                }
            }
        });



        Utilities.hideKeyboard(MainActivity.this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
          fileUploadApplication.setCurrentViewPage(position);
          fileUploadApplication.setViewPageAfterCloseCamera(position);
      }

      @Override
      public void onPageSelected(int position) {
          fileUploadApplication.setCurrentViewPage(position);
          fileUploadApplication.setViewPageAfterCloseCamera(position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        int tabsize = ((MainPageFragmentAdapter) viewPager.getAdapter()).getTabSize();

        outState.putInt("tabSize", tabsize);

    }

    private void removeSelectedAttachment() {

        int selectedIndex = viewPager.getCurrentItem();
        ((MainPageFragmentAdapter) viewPager.getAdapter()).removeFragmentAtIndex(selectedIndex);
        tabLayout.setViewPager(viewPager);

        ((MainPageFragmentAdapter) viewPager.getAdapter()).notifyDataSetChanged();

    }

    private void uploadFiles(List<SelectedFileModel> selectedFileModels) {

        UploadDialog.create(executorService,
                selectedFileModels,
                ((EditText) findViewById(R.id.edtTxtPatientId)).getText().toString(),
                Utilities.getSharedPreferences(this).getString("user_id", ""), this)
                .show(getSupportFragmentManager(), "Upload Dialog");
    }


    private void loadData(final String patiendId) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching user data");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    Call<PatientData> call = Utilities.getWebApi(MainActivity.this).getPatientData(patiendId);
                    call.enqueue(new Callback<PatientData>() {
                        @Override
                        public void onResponse(Call<PatientData> call, Response<PatientData> response) {
                            progressDialog.dismiss();
                            if (response == null) {
                                Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                            }else if (response.body() == null) {
                                 Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                            }else if (response.body().getPatName() == null) {
                                 Toast.makeText(MainActivity.this, "Invalid patient id", Toast.LENGTH_SHORT).show();
                            }else{
                                   initUiWithData(response.body());
                                 }
                        }
                        @Override
                        public void onFailure(Call<PatientData> call, Throwable t) {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error", MainActivity.this);
                        }
                    });

                } catch (Exception e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        }).start();

//                    final Response<PatientData> response = Utilities.getWebApi(MainActivity.this).getPatientData(patiendId).execute();

//                    runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            progressDialog.dismiss();
//                            Log.e("","response.body().getPatId()"+response.body().getPatId());
//                            Log.e("","response.body().getPatSex()"+response.body().getPatSex());
//                            Log.e("","response.body().getPatName()"+response.body().getPatName());
//                            Log.e("","response.body().getPatSname()"+response.body().getPatSname());
//                            if (response == null) {
//                                Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();
//
//                            } else if (response.body() == null) {
//                                Toast.makeText(MainActivity.this, "Network error", Toast.LENGTH_SHORT).show();
//
//                            } else if (response.body().getPatId() == null) {
//                                Toast.makeText(MainActivity.this, "Invalid patient id", Toast.LENGTH_SHORT).show();
//                            } else {
//                                initUiWithData(response.body());
//                            }
//
//                        }
//                    });

    }

    private void initUiWithData(PatientData body) {

        this.patientData = body;

        ((TextView) findViewById(R.id.txtUserId)).setText(body.getUsrId());
        ((TextView) findViewById(R.id.txtUsername)).setText(body.getPatName());
        ((TextView) findViewById(R.id.txtGender)).setText(body.getPatSex().contains("F") ? "Female" : "Male");
        findViewById(R.id.txtUsername).setSelected(true);

    }

    @Override
    public void onCompletedAllUploads() {
        if (showcompleteFlag)
            Utilities.showSnackBar("Completed all uploads", MainActivity.this);
        removeAllFileFragments();
    }

    @Override
    public void onFailed() {

    }

    private void removeAllFileFragments() {
        ((MainPageFragmentAdapter) viewPager.getAdapter()).clearFragments();
        viewPager.removeAllViews();
        viewPager.setAdapter(null);
        viewPager.setAdapter(new MainPageFragmentAdapter(getSupportFragmentManager(), this));
        tabLayout.setViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                SharedPreferences.Editor prefEditor = Utilities.getSharedPreferences(MainActivity.this).edit();
                                prefEditor.remove("user_id");
                                prefEditor.remove("user_name");
                                prefEditor.remove("user_type");
                                prefEditor.commit();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Logout");
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utilities.showSnackBar("Please click BACK again to exit", MainActivity.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        fileUploadApplication.setViewPageAfterCloseCamera(viewPager.getCurrentItem());
        try {
            if (requestCode == FileFragment.TAKE_PICTURE && resultCode == Activity.RESULT_OK) {

Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (fileUploadApplication.getOnCapturCompleteListener() != null)
                            fileUploadApplication.getOnCapturCompleteListener().onComplete();
                    }
                },500 );


            }
            super.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
