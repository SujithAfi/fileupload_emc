package com.afiinfotech.fileuploader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.afiinfotech.fileuploader.Interface.searchAdapterClicked;
import com.afiinfotech.fileuploader.R;
import com.afiinfotech.fileuploader.Utilis.Utilities;

import java.util.ArrayList;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class SearchDetailsAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<String> patientList;
    private ArrayList<String> suggestions;
    private searchAdapterClicked mListner;
    private Boolean isNameSearch;
    @Override
    public Filter getFilter() {
        return filter;
    }


    public SearchDetailsAdapter(Context context, ArrayList<String> temp_array,Boolean isNameSearch) {

        mContext = context;
        patientList = temp_array;
        this.isNameSearch = isNameSearch;
    }


    @Override
    public int getCount() {

        return patientList.size();
    }

    @Override
    public Object getItem(int position) {
        return patientList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_textview,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.tvPatientDetails = (TextView) convertView.findViewById(R.id.tvPatiantDetail);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvPatientDetails.setText(patientList.get(position).toString());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListner = Utilities.getSearchAdapterListner();
                if(mListner != null){
                    mListner.searchAdapterClicked(position,isNameSearch);
                }
            }
        });
        return convertView;
    }


    Filter filter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (patientList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < patientList.size(); i++) {
                    if (patientList.get(i).toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                        suggestions.add(patientList.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            }
        }
    };
        private class ViewHolder{
             public TextView tvPatientDetails;
    }

}
