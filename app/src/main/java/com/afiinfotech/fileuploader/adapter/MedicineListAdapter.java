package com.afiinfotech.fileuploader.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.afiinfotech.fileuploader.Interface.searchAdapterClicked;
import com.afiinfotech.fileuploader.R;
import com.afiinfotech.fileuploader.Utilis.Utilities;

import java.util.ArrayList;

/**
 * Created by QDES Infotech on 11/4/2016.
 */

public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<String> meicineList;
    private searchAdapterClicked mListner;
    class ViewHolder extends RecyclerView.ViewHolder {
        public CheckBox checkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            checkbox = (CheckBox) itemView.findViewById(R.id.cbEmcListItems);
        }
    }

    public MedicineListAdapter(Context context, ArrayList<String> temp_array) {
        mContext = context;
        meicineList = temp_array;
        Log.e("ProcedureListAdapter", "procedureList" + temp_array);
//        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_checkbox, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.checkbox.setText(meicineList.get(position).toString());
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListner = Utilities.getSearchAdapterListner();
                if(mListner != null){
                    mListner.onCheckBoxClickListner(position,holder.checkbox.isChecked(),true);
                }
            }
        });
    }



    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return meicineList.size();
    }



}