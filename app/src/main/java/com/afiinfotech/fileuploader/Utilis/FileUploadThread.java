package com.afiinfotech.fileuploader.Utilis;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.util.TimeUtils;

import com.afiinfotech.fileuploader.Model.SelectedFileModel;
import com.afiinfotech.fileuploader.Model.UploadResponse;
import com.google.gson.Gson;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by afi-mac-001 on 07/06/16.
 */
public class FileUploadThread implements Runnable {


    private final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(70, TimeUnit.SECONDS)
            .build();

    private Context context;
    private Handler uiHandler;
    private UploadProgressListner uploadProgressListner;
    private SelectedFileModel selectedFileModel;
    private String userId;
    private String patientId;

    public FileUploadThread(Handler uiHandler, UploadProgressListner uploadProgressListner, SelectedFileModel selectedFileModel, String userId, String patientId, Context c) {
        this.uiHandler = uiHandler;
        this.uploadProgressListner = uploadProgressListner;
        this.selectedFileModel = selectedFileModel;
        this.userId = userId;
        this.patientId = patientId;
        this.context = c;
    }

    private FileUploadThread() {
    }

    @Override
    public void run() {
        CountingFileRequestBody imageBody = null;
        try {
            File file = new File(selectedFileModel.getFilePath());

            final long totalSize = file.length();

            imageBody = new CountingFileRequestBody(file, selectedFileModel.getFileMimeType(), new CountingFileRequestBody.ProgressListener() {
                @Override
                public void transferred(final long num) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            uploadProgressListner.onItemProgressChanged(selectedFileModel.getId(), (int) ((num / (float) totalSize) * 100));
                        }
                    });
                }
            });

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            if (imageBody != null)
                builder.addFormDataPart("contents", FilenameUtils.getName(selectedFileModel.getFileName()), imageBody);

            builder.addFormDataPart("description", selectedFileModel.getFileDescription());
            builder.addFormDataPart("user_id", this.userId);
            builder.addFormDataPart("patient_id", this.patientId);
            builder.addFormDataPart("doc_type", selectedFileModel.getFileTypeCode());
            builder.addFormDataPart("file_type", selectedFileModel.getFileExtention());

            Log.e("","selectedFileModel.getFileTypeCode()"+selectedFileModel.getFileTypeCode());
            Log.e("","selectedFileModel.getFileExtention()"+selectedFileModel.getFileExtention());
            MultipartBody requestBody = builder.build();

            Request request = new okhttp3.Request.Builder()
                    .url(Utilities.getWebApiBaseUrl(context) + "api/Upload")
                    .post(requestBody)
                    .removeHeader("Content-Length")
                    .build();


            Call call = okHttpClient.newCall(request);

                Response response = call.execute();
                String resp = response.body().string();

            final UploadResponse uploadResponse = new Gson().fromJson(resp, UploadResponse.class);
            Log.e("File Upload Thread","uploadResponse"+uploadResponse.message);
                    if (uploadResponse.isSucceeded) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                uploadProgressListner.onItemCompleted(selectedFileModel.getId());
                            }
                        });
                    } else {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                Log.e("File Upload thread","uiHandler.post else***");

                                uploadProgressListner.onItemUploadError(selectedFileModel.getId());
                            }
                        });
                    }


        } catch (Exception e) {
            e.printStackTrace();
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("File Upload thread","Exception***");
                    uploadProgressListner.onItemUploadError(selectedFileModel.getId());
                }
            });
        }
    }
}
