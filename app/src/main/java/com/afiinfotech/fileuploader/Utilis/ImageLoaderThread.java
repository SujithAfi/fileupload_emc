package com.afiinfotech.fileuploader.Utilis;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by afi-mac-001 on 09/06/16.
 */
public class ImageLoaderThread implements Runnable {

    private Handler uiHandler;
    private ImageView imageView;
    private String filepth;
    private TextView textView;

    public ImageLoaderThread(Handler uiHandler, ImageView imageView, String filepth, TextView textView) {
        this.uiHandler = uiHandler;
        this.imageView = imageView;
        this.filepth = filepth;
        this.textView = textView;
    }

    @Override
    public void run() {

        try {
            Log.e("Image Loader", "run");
            String filePath = this.filepth;
            final File file = new File(filePath);
            textView.setText(file.getName());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = false;
            options.inSampleSize = 8;
            Bitmap bitmapResized;
            Bitmap bitmapDecoded = BitmapFactory.decodeFile(filePath, options);
            long length = file.length();
            //Convert image size to MB
            length = length/1048576;
            Log.e("Image Size","length"+length);
            if(length > 10) {
                ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                bitmapDecoded.compress(Bitmap.CompressFormat.PNG, 40, bytearrayoutputstream);
                byte[] bytes = bytearrayoutputstream.toByteArray();
                bitmapDecoded = BitmapFactory.decodeByteArray(bytes,0,bytes.length);

            }
            final Bitmap scaled = Bitmap.createScaledBitmap(bitmapDecoded, 120, 120, true);
            length = scaled.getByteCount()/1024;
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageBitmap(scaled);
//                    textView.setText(file.getName());
                }
            });
        } catch (Exception e) {
        }

    }

}
