package com.afiinfotech.fileuploader.Utilis;

import com.afiinfotech.fileuploader.Model.LoginModel;
import com.afiinfotech.fileuploader.Model.MedicineList;
import com.afiinfotech.fileuploader.Model.PatientData;
import com.afiinfotech.fileuploader.Model.ProcedureList;
import com.afiinfotech.fileuploader.Model.SaveFileModel;
import com.afiinfotech.fileuploader.Model.SaveResultModel;
import com.afiinfotech.fileuploader.Model.SearchResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public interface WebApi {

    @POST("User/Login")
    Call<LoginModel> login(@Query("user")String username, @Query("pwd")String password);

    @POST("Patient/GetPatDetails")
    Call<PatientData> getPatientData(@Query("patId") String id);

    @POST("Patient/SearchPatient")
    Call<List<SearchResult>> searchPatient(@Query("SearchString") String SearchString);

    @POST("Procedure/ProcedureList")
    Call<List<ProcedureList>> getProcedureList(@Query("cons_id") String cons_id);

    @POST("Medicine/MedicineList")
    Call<List<MedicineList>> getMedicineList(@Query("cons_id") String cons_id);

    @POST("Save/SaveData")
    Call<SaveResultModel> saveData(@Body SaveFileModel saveFileModel);

    @GET("checkApi")
    Call<Double> checkWebApi(@Query("first") double first, @Query("second") double second);

    @POST("Save/Sign/Upload")
    Call<SaveResultModel> uploadSign(@Body SaveFileModel saveFileModel);




}
