package com.afiinfotech.fileuploader;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afiinfotech.fileuploader.Interface.searchAdapterClicked;
import com.afiinfotech.fileuploader.Model.MedicineList;
import com.afiinfotech.fileuploader.Model.ProcedureList;
import com.afiinfotech.fileuploader.Model.ProcedureSelectedItemsList;
import com.afiinfotech.fileuploader.Model.SaveFileModel;
import com.afiinfotech.fileuploader.Model.SaveResultModel;
import com.afiinfotech.fileuploader.Model.SearchResult;
import com.afiinfotech.fileuploader.Utilis.Utilities;
import com.afiinfotech.fileuploader.adapter.MedicineListAdapter;
import com.afiinfotech.fileuploader.adapter.ProcedureListAdapter;
import com.afiinfotech.fileuploader.adapter.SearchDetailsAdapter;
import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConsultationActivity extends AppCompatActivity implements searchAdapterClicked,View.OnClickListener{
    private RecyclerView gvPrec_Procedure,gvSelling_Product;
    private AutoCompleteTextView tvPtName,tvFileNo;
    private Toolbar toolBar;
    private ArrayList<ProcedureList> procedureArrayList;
    private ArrayList<MedicineList> medicineArrayList;
    private ArrayList<String> medicineCheckBoxIds = new ArrayList<String>();
    private ArrayList<String> procedureCheckBoxIds = new ArrayList<String>();
    private ArrayList<String> temp_array;
    private ArrayList<SearchResult> result;
    private SignatureView signConsult,signPatient;
    private Button btnSubmit, btnClearConsltSign, btnClearPatSign;
    private TextView tvConsultName,tvConsultDate,tvDate;
    private final Calendar calendar = Calendar.getInstance();
    private Boolean isNameSearch;
    private String LocId,UserId;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation);
        Utilities.setSearchAdapterListner((searchAdapterClicked) this);

        tvPtName = (AutoCompleteTextView)findViewById(R.id.tvPtName);
        tvFileNo = (AutoCompleteTextView)findViewById(R.id.tvFileNo);
        tvConsultName = (TextView) findViewById(R.id.tvConName);
        tvConsultDate = (TextView) findViewById(R.id.tvConsultDate);
        tvDate = (TextView) findViewById(R.id.tvDate);

        signConsult = (SignatureView) findViewById(R.id.signature_view);
        signPatient = (SignatureView) findViewById(R.id.signature_view2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnClearConsltSign = (Button) findViewById(R.id.btnClearConsultSign);
        btnClearPatSign = (Button) findViewById(R.id.btnClearPatSign);

        tvConsultName.setText(Utilities.getSharedPreferences(this).getString("Cons_Name", ""));

        intilizeDateFields(calendar.getTime(),tvDate);
        intilizeDateFields(calendar.getTime(),tvConsultDate);

        btnSubmit.setOnClickListener(this);
        btnClearConsltSign.setOnClickListener(this);
        btnClearPatSign.setOnClickListener(this);
        tvConsultDate.setOnClickListener(this);

        toolBar = (Toolbar) findViewById(R.id.tbEmc);
        setSupportActionBar(toolBar);
        toolBar.setSubtitle("User :" + Utilities.getSharedPreferences(this).getString("user_name", ""));

        gvPrec_Procedure = (RecyclerView) findViewById(R.id.gvPresc_Procedure);
        gvSelling_Product = (RecyclerView) findViewById(R.id.gvSelling_Product);
        gvPrec_Procedure.setNestedScrollingEnabled(false);
        gvSelling_Product.setNestedScrollingEnabled(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(this,2);
        gvPrec_Procedure.setLayoutManager(gridLayoutManager);
        gvSelling_Product.setLayoutManager(gridLayoutManager1);


        progressDialog = new ProgressDialog(ConsultationActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data");
        progressDialog.show();

        loadProcedureList();
        loadMedicineList();



        tvFileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ((s != null) && (tvFileNo.hasFocus())) {
                    if(isNameSearch != null && !isNameSearch) {
                        isNameSearch = true;
                    }
                    else{
                        Call<List<SearchResult>> call = Utilities.getWebApi(ConsultationActivity.this).searchPatient(s.toString());
                        call.enqueue(new Callback<List<SearchResult>>() {
                            @Override
                            public void onResponse(Call<List<SearchResult>> call, Response<List<SearchResult>> response) {
                                if (response != null) {
                                    if (response.body() != null) {
                                        result = (ArrayList<SearchResult>) (response.body());
                                        ArrayList<String> temp_array = new ArrayList<String>();
                                        temp_array.clear();
                                        for (int i = 0; i < result.size(); i++) {
                                            temp_array.add(i, result.get(i).getPatId().toString());
                                        }
                                        SearchDetailsAdapter sAdapter = (new SearchDetailsAdapter(ConsultationActivity.this, temp_array, false));
                                        tvFileNo.setAdapter(sAdapter);
                                        sAdapter.notifyDataSetChanged();


                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<List<SearchResult>> call, Throwable t) {
                                Utilities.showSnackBar("Network error", ConsultationActivity.this);
                            }
                        });
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvPtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && tvPtName.hasFocus()) {
                    if (isNameSearch != null && isNameSearch) {
                        isNameSearch = false;
                    } else {
                        Call<List<SearchResult>> call = Utilities.getWebApi(ConsultationActivity.this).searchPatient(s.toString());
                        call.enqueue(new Callback<List<SearchResult>>() {
                            @Override
                            public void onResponse(Call<List<SearchResult>> call, Response<List<SearchResult>> response) {
                                if (response != null) {
                                    if (response.body() != null) {
                                        result = (ArrayList<SearchResult>) (response.body());
                                        ArrayList<String> temp_array = new ArrayList<String>();
                                        temp_array.clear();
                                        for (int i = 0; i < result.size(); i++) {
                                            temp_array.add(i, result.get(i).getPatName().toString());
                                        }

                                        SearchDetailsAdapter sAdapter = (new SearchDetailsAdapter(ConsultationActivity.this, temp_array, true));
                                        tvPtName.setAdapter(sAdapter);
                                        sAdapter.notifyDataSetChanged();


                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<List<SearchResult>> call, Throwable t) {
                                Utilities.showSnackBar("Network error", ConsultationActivity.this);
                            }
                        });
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                SharedPreferences.Editor prefEditor = Utilities.getSharedPreferences(ConsultationActivity.this).edit();
                                prefEditor.remove("user_id");
                                prefEditor.remove("user_name");
                                prefEditor.remove("user_type");
                                prefEditor.remove("cons_id");
                                prefEditor.remove("Cons_Name");
                                prefEditor.commit();
                                startActivity(new Intent(ConsultationActivity.this, LoginActivity.class));
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(ConsultationActivity.this);
                builder.setTitle("Logout");
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadProcedureList(){

        Call<List<ProcedureList>> call = Utilities.getWebApi(ConsultationActivity.this).getProcedureList(Utilities.getSharedPreferences(this).getString("cons_id", ""));
        call.enqueue(new Callback<List<ProcedureList>>() {
            @Override
            public void onResponse(Call<List<ProcedureList>> call, Response<List<ProcedureList>> response) {

                Log.e("*******","response"+response);
                if(response!=null){
                    Log.e("*******","response.body()"+response.body());
                    if(response.body()!=null){
                        procedureArrayList = (ArrayList<ProcedureList>) (response.body());
                        temp_array = new ArrayList<String>();
                        temp_array.clear();
                        for (int i = 0; i < procedureArrayList.size(); i++) {
                            temp_array.add(i, procedureArrayList.get(i).getCpName());
                        }
                        Log.e("*******","temp_array"+temp_array);
                        gvPrec_Procedure.setAdapter(new ProcedureListAdapter(ConsultationActivity.this,temp_array));
                    }else{
                        Toast.makeText(ConsultationActivity.this, "Procedure list data not available", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Utilities.showSnackBar("Network error", ConsultationActivity.this);
                }
            }

            @Override
            public void onFailure(Call<List<ProcedureList>> call, Throwable t) {
                Utilities.showSnackBar("Network error", ConsultationActivity.this);
            }

        });


    }
    private void loadMedicineList(){
        Call<List<MedicineList>> call = Utilities.getWebApi(ConsultationActivity.this).getMedicineList(Utilities.getSharedPreferences(this).getString("cons_id", ""));
        call.enqueue(new Callback<List<MedicineList>>() {
            @Override
            public void onResponse(Call<List<MedicineList>> call, Response<List<MedicineList>> response) {

                if(response!=null){
                    if(response.body()!=null){
                        medicineArrayList = (ArrayList<MedicineList>) (response.body());
                        temp_array = new ArrayList<String>();
                        temp_array.clear();
                        for (int i = 0; i < medicineArrayList.size(); i++) {
                            temp_array.add(i, medicineArrayList.get(i).getItmName());
                        }
                        gvSelling_Product.setAdapter(new MedicineListAdapter(ConsultationActivity.this,temp_array));
                        if(progressDialog!=null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }else{
                        if(progressDialog!=null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(ConsultationActivity.this, "Medicine list data not available", Toast.LENGTH_SHORT).show();
                    }
                }   else{
                    Utilities.showSnackBar("Network error", ConsultationActivity.this);
                    if(progressDialog!=null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<MedicineList>> call, Throwable t) {
                if(progressDialog!=null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Utilities.showSnackBar("Network error", ConsultationActivity.this);
            }

        });
    }

    @Override
    public void searchAdapterClicked(int position,Boolean isNameSearch) {
        Log.e("isNameSearch","isNameSearch"+isNameSearch);
        Log.e("isNameSearch","result.get(position)"+result.size());
        this.isNameSearch = isNameSearch;
        if(result != null){
            tvFileNo.setText(result.get(position).getPatId().toString());
            tvPtName.setText(result.get(position).getPatName().toString());
            LocId = result.get(position).getLocId().toString();
            UserId = result.get(position).getUsrId().toString();
        }
    }

    @Override
    public void onCheckBoxClickListner(int position, boolean isChecked, boolean isMedicineList) {
        Log.e("onCheckBoxClickListner","isMedicineList"+isMedicineList);
        if(isMedicineList) {
            if(isChecked) {
                medicineCheckBoxIds.add(medicineArrayList.get(position).getItmCd());
            }else{
                medicineCheckBoxIds.remove(medicineArrayList.get(position).getItmCd());
            }
        }else{
            if(isChecked) {
                procedureCheckBoxIds.add(medicineArrayList.get(position).getItmCd());
            }else{
                procedureCheckBoxIds.remove(medicineArrayList.get(position).getItmCd());
            }
        }
        Log.e("medicineCheckBoxIds","medicineCheckBoxIds"+medicineCheckBoxIds);
        Log.e("procedureCheckBoxIds","procedureCheckBoxIds"+procedureCheckBoxIds);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                saveData();
                break;
            case R.id.btnClearConsultSign:
                signConsult.clearCanvas();
                break;
            case R.id.btnClearPatSign:
                signPatient.clearCanvas();
                break;
            case R.id.tvConsultDate:
                loadTimePickerDialog();
                break;
            default:
                break;
        }
    }



    private void saveData() {
        if(tvPtName.getText().toString().trim().length() == 0){
            tvPtName.setError("Enter Patient Name");
        }else if(tvFileNo.getText().toString().trim().length() == 0){
            tvFileNo.setError("Enter File Number");
        }else if(signConsult.getSignatureBitmap()==null || signPatient.getSignatureBitmap() == null){
            Toast.makeText(ConsultationActivity.this, "Signature fields should not blank", Toast.LENGTH_SHORT).show();
        }
        else {
            final ProgressDialog saveProgressDialog = new ProgressDialog(ConsultationActivity.this);
            saveProgressDialog.setIndeterminate(true);
            saveProgressDialog.setCancelable(false);
            saveProgressDialog.setMessage("Saving Data");
            saveProgressDialog.show();
            SaveFileModel saveFile = new SaveFileModel();
            saveFile.setConsId(Utilities.getSharedPreferences(this).getString("cons_id", ""));
            saveFile.setLmDt(calendar.getTime());
            saveFile.setLmUsr(Utilities.getSharedPreferences(this).getString("user_name", ""));
            saveFile.setMode("ADD");
            saveFile.setLocId(LocId);
            saveFile.setPatId(tvFileNo.getText().toString());
            saveFile.setPatName(tvPtName.getText().toString());
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date date = formatter.parse(tvConsultDate.getText().toString());
                saveFile.setPRTransAppDate(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            saveFile.setPRTransTime(calendar.getTime());//system time and date
            saveFile.setPRTransNotes("");//null
            saveFile.setPRTransId("1");//

            saveFile.setPRTransDate(calendar.getTime());
            saveFile.setPRTransCompDate(calendar.getTime());
            saveFile.setPRTransCancelReason("");
            saveFile.setPRTransCancelled("");

            List<ProcedureSelectedItemsList> procedureList = new ArrayList<ProcedureSelectedItemsList>();
            if (medicineCheckBoxIds != null) {
                for (int i = 0; i < medicineCheckBoxIds.size(); i++) {
                    ProcedureSelectedItemsList objProcedureList = new ProcedureSelectedItemsList();
                    objProcedureList.setItmCd(medicineCheckBoxIds.get(i).toString());
                    objProcedureList.setTestCd("");
                    objProcedureList.setLocId(LocId);
                    objProcedureList.setPRTransDType("M");
                    objProcedureList.setPRTransDCtr("1");
                    objProcedureList.setPRTransId("1");
                    objProcedureList.setPRTransDNotes("");
                    objProcedureList.setPRTransDCancelled("");
                    objProcedureList.setBillYear(null);
                    objProcedureList.setBillNo(null);
                    procedureList.add(objProcedureList);
                }
            }
            if (procedureCheckBoxIds != null) {
                for (int i = 0; i < procedureCheckBoxIds.size(); i++) {
                    ProcedureSelectedItemsList objProcedureList = new ProcedureSelectedItemsList();
                    objProcedureList.setItmCd("");
                    objProcedureList.setTestCd(procedureCheckBoxIds.get(i).toString());
                    objProcedureList.setLocId(LocId);
                    objProcedureList.setPRTransDType("L");
                    objProcedureList.setPRTransDCtr("1");
                    objProcedureList.setPRTransId("1");
                    objProcedureList.setPRTransDNotes("");
                    objProcedureList.setPRTransDCancelled("");
                    objProcedureList.setBillYear(null);
                    objProcedureList.setBillNo(null);
                    procedureList.add(objProcedureList);
                }
            }
            saveFile.setPRTransDVOList(procedureList);
            saveFile.setPatSign(signatureByteFormat(signPatient.getSignatureBitmap()));
            saveFile.setPRSign(signatureByteFormat(signConsult.getSignatureBitmap()));

           /* try {
                Utilities.getWebApi(ConsultationActivity.this).saveData(saveFile).enqueue(new Callback<SaveResultModel>() {
                    @Override
                    public void onResponse(Call<SaveResultModel> call, Response<SaveResultModel> response) {
                        if (response != null) {
                            if (response.body() != null) {
                                if (response.body().getMessage().toString().equals("Saved Successfully..!")) {
                                    Utilities.getWebApi(ConsultationActivity.this).uploadSign(saveFile).enqueue(new Callback<SaveResultModel>() {
                                    @Override
                                    public void onResponse(Call<SaveResultModel> call, Response<SaveResultModel> response) {
                                        if (response != null) {
                                            if (response.body() != null) {
                                                if (response.body().getMessage().toString().equals("Saved Successfully")) {
                                                    saveProgressDialog.dismiss();
                                                    Utilities.showSnackBar("Save your data!", ConsultationActivity.this);
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SaveResultModel> call, Throwable t) {

                                  }
                              });

                                            Log.e("", "response.body().getMessage().toString()" + response.body().getMessage().toString());
                                } else {
                                    Utilities.showSnackBar("Couldn't save your data!", ConsultationActivity.this);
                                }
                            } else {
                                Utilities.showSnackBar("Couldn't save your data!", ConsultationActivity.this);
                            }
                        } else {
                            Utilities.showSnackBar("Network error", ConsultationActivity.this);
                        }
                        if(saveProgressDialog!=null && saveProgressDialog.isShowing()) {
                            saveProgressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<SaveResultModel> call, Throwable t) {

                    }

                });
            } catch (Exception e) {
                if(saveProgressDialog!=null && saveProgressDialog.isShowing()) {
                    saveProgressDialog.dismiss();
                }
                e.printStackTrace();
            }*/
        }
    }

    private String signatureByteFormat(Bitmap signatureBitmap) {
        Bitmap bmp = signatureBitmap;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Log.e("byteArray",""+byteArray);
        return byteArray.toString();
    }

    private void loadTimePickerDialog() {
        intilizeDateFields(calendar.getTime(),tvConsultDate);
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        intilizeDateFields(calendar.getTime(),tvConsultDate);
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        dpd.show(getFragmentManager(), "Datepickerdialog");
    }
    /**
     * Intilize the date textviews
     *
     * @param date
     */
    private void intilizeDateFields(Date date,TextView tv) {
        Log.e("intilizeDateFields","date"+date);
        //tv.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        tv.setText("");
        tv.setText(tv.getText()+" "+new SimpleDateFormat("dd").format(date));
        tv.setText(tv.getText()+" "+new SimpleDateFormat("MMM").format(date).toUpperCase());
        tv.setText(tv.getText()+" "+new SimpleDateFormat("yyyy").format(date));
    }
}
