package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 11/8/2016.
 */

public class MedicineList {
    @SerializedName("Itm_Cd")
    @Expose
    private String itmCd;
    @SerializedName("Itm_Name")
    @Expose
    private String itmName;

    /**
     *
     * @return
     * The itmCd
     */
    public String getItmCd() {
        return itmCd;
    }

    /**
     *
     * @param itmCd
     * The Itm_Cd
     */
    public void setItmCd(String itmCd) {
        this.itmCd = itmCd;
    }

    /**
     *
     * @return
     * The itmName
     */
    public String getItmName() {
        return itmName;
    }

    /**
     *
     * @param itmName
     * The Itm_Name
     */
    public void setItmName(String itmName) {
        this.itmName = itmName;
    }

}
