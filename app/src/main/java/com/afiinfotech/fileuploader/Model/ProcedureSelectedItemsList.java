package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 11/9/2016.
 */

public class ProcedureSelectedItemsList {
    private String PRTrans_Id;
    private String Loc_Id;
    private String PRTransD_Ctr;
    private String Itm_Cd;
    private String Test_Cd;
    private String PRTransD_Type;
    private String PRTransD_Notes;
    private String PRTransD_Cancelled;
    private String Bill_No;
    private String Bill_Year;

    /**
     *
     * @return
     * The pRTransId
     */
    public String getPRTransId() {
        return PRTrans_Id;
    }

    /**
     *
     * @param pRTransId
     * The PRTrans_Id
     */
    public void setPRTransId(String pRTransId) {
        this.PRTrans_Id = pRTransId;
    }

    /**
     *
     * @return
     * The locId
     */
    public String getLocId() {
        return Loc_Id;
    }

    /**
     *
     * @param locId
     * The Loc_Id
     */
    public void setLocId(String locId) {
        this.Loc_Id = locId;
    }

    /**
     *
     * @return
     * The pRTransDCtr
     */
    public String getPRTransDCtr() {
        return PRTransD_Ctr;
    }

    /**
     *
     * @param pRTransDCtr
     * The PRTransD_Ctr
     */
    public void setPRTransDCtr(String pRTransDCtr) {
        this.PRTransD_Ctr = pRTransDCtr;
    }

    /**
     *
     * @return
     * The itmCd
     */
    public String getItmCd() {
        return Itm_Cd;
    }

    /**
     *
     * @param itmCd
     * The Itm_Cd
     */
    public void setItmCd(String itmCd) {
        this.Itm_Cd = itmCd;
    }

    /**
     *
     * @return
     * The testCd
     */
    public String getTestCd() {
        return Test_Cd;
    }

    /**
     *
     * @param testCd
     * The Test_Cd
     */
    public void setTestCd(String testCd) {
        this.Test_Cd = testCd;
    }

    /**
     *
     * @return
     * The pRTransDType
     */
    public String getPRTransDType() {
        return PRTransD_Type;
    }

    /**
     *
     * @param pRTransDType
     * The PRTransD_Type
     */
    public void setPRTransDType(String pRTransDType) {
        this.PRTransD_Type = pRTransDType;
    }

    /**
     *
     * @return
     * The pRTransDNotes
     */
    public String getPRTransDNotes() {
        return PRTransD_Notes;
    }

    /**
     *
     * @param pRTransDNotes
     * The PRTransD_Notes
     */
    public void setPRTransDNotes(String pRTransDNotes) {
        this.PRTransD_Notes = pRTransDNotes;
    }

    /**
     *
     * @return
     * The pRTransDCancelled
     */
    public String getPRTransDCancelled() {
        return PRTransD_Cancelled;
    }

    /**
     *
     * @param pRTransDCancelled
     * The PRTransD_Cancelled
     */
    public void setPRTransDCancelled(String pRTransDCancelled) {
        this.PRTransD_Cancelled = pRTransDCancelled;
    }

    /**
     *
     * @return
     * The billNo
     */
    public String getBillNo() {
        return Bill_No;
    }

    /**
     *
     * @param billNo
     * The Bill_No
     */
    public void setBillNo(String billNo) {
        this.Bill_No = billNo;
    }

    /**
     *
     * @return
     * The billYear
     */
    public String getBillYear() {
        return Bill_Year;
    }

    /**
     *
     * @param billYear
     * The Bill_Year
     */
    public void setBillYear(String billYear) {
        this.Bill_Year = billYear;
    }

}
