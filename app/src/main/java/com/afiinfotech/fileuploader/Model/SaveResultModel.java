package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 11/9/2016.
 */

public class SaveResultModel {
    private String Message;
    private String ID;
    private Boolean result;

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return Message;
    }

    /**
     *
     * @param message
     * The Message
     */
    public void setMessage(String message) {
        this.Message = message;
    }

    /**
     *
     * @return
     * The iD
     */
    public String getID() {
        return ID;
    }

    /**
     *
     * @param iD
     * The ID
     */
    public void setID(String iD) {
        this.ID = iD;
    }

    /**
     *
     * @return
     * The result
     */
    public Boolean getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

}
