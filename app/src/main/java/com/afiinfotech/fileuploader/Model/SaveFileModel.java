package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by QDES Infotech on 11/9/2016.
 */

public class SaveFileModel {


    private String PRTrans_Id;
    private String Loc_Id;
    private Date PRTrans_Date;
    private Date PRTrans_Time;
    private String pat_Id;
    private String Pat_Name;
    private Date PRTrans_AppDate;
    private Date PRTrans_CompDate;
    private String Cons_Id;
    private String PRTrans_Notes;
    private String PRTrans_Cancelled;
    private String PRTrans__Cancel_Reason;
    private String Lm_Usr;
    private Date Lm_Dt;
    private String Pat_Sign;
    private String PR_Sign;
    private String Mode;
    private List<ProcedureSelectedItemsList> PRTransDVOList = new ArrayList<ProcedureSelectedItemsList>();

    /**
     *
     * @return
     * The pRTransId
     */
    public String getPRTransId() {
        return PRTrans_Id;
    }

    /**
     *
     * @param pRTransId
     * The PRTrans_Id
     */
    public void setPRTransId(String pRTransId) {
        this.PRTrans_Id = pRTransId;
    }

    /**
     *
     * @return
     * The locId
     */
    public String getLocId() {
        return Loc_Id;
    }

    /**
     *
     * @param locId
     * The Loc_Id
     */
    public void setLocId(String locId) {
        this.Loc_Id = locId;
    }

    /**
     *
     * @return
     * The pRTransDate
     */
    public Date getPRTransDate() {
        return PRTrans_Date;
    }

    /**
     *
     * @param pRTransDate
     * The PRTrans_Date
     */
    public void setPRTransDate(Date pRTransDate) {
        this.PRTrans_Date = pRTransDate;
    }

    /**
     *
     * @return
     * The pRTransTime
     */
    public Date getPRTransTime() {
        return PRTrans_Time;
    }

    /**
     *
     * @param pRTransTime
     * The PRTrans_Time
     */
    public void setPRTransTime(Date pRTransTime) {
        this.PRTrans_Time = pRTransTime;
    }

    /**
     *
     * @return
     * The patId
     */
    public String getPatId() {
        return pat_Id;
    }

    /**
     *
     * @param patId
     * The pat_Id
     */
    public void setPatId(String patId) {
        this.pat_Id = patId;
    }

    /**
     *
     * @return
     * The patName
     */
    public String getPatName() {
        return Pat_Name;
    }

    /**
     *
     * @param patName
     * The Pat_Name
     */
    public void setPatName(String patName) {
        this.Pat_Name = patName;
    }

    /**
     *
     * @return
     * The pRTransAppDate
     */
    public Date getPRTransAppDate() {
        return PRTrans_AppDate;
    }

    /**
     *
     * @param pRTransAppDate
     * The PRTrans_AppDate
     */
    public void setPRTransAppDate(Date pRTransAppDate) {
        this.PRTrans_AppDate = pRTransAppDate;
    }

    /**
     *
     * @return
     * The pRTransCompDate
     */
    public Date getPRTransCompDate() {
        return PRTrans_CompDate;
    }

    /**
     *
     * @param pRTransCompDate
     * The PRTrans_CompDate
     */
    public void setPRTransCompDate(Date pRTransCompDate) {
        this.PRTrans_CompDate = pRTransCompDate;
    }

    /**
     *
     * @return
     * The consId
     */
    public String getConsId() {
        return Cons_Id;
    }

    /**
     *
     * @param consId
     * The Cons_Id
     */
    public void setConsId(String consId) {
        this.Cons_Id = consId;
    }

    /**
     *
     * @return
     * The pRTransNotes
     */
    public String getPRTransNotes() {
        return PRTrans_Notes;
    }

    /**
     *
     * @param pRTransNotes
     * The PRTrans_Notes
     */
    public void setPRTransNotes(String pRTransNotes) {
        this.PRTrans_Notes = pRTransNotes;
    }

    /**
     *
     * @return
     * The pRTransCancelled
     */
    public String getPRTransCancelled() {
        return PRTrans_Cancelled;
    }

    /**
     *
     * @param pRTransCancelled
     * The PRTrans_Cancelled
     */
    public void setPRTransCancelled(String pRTransCancelled) {
        this.PRTrans_Cancelled = pRTransCancelled;
    }

    /**
     *
     * @return
     * The pRTransCancelReason
     */
    public String getPRTransCancelReason() {
        return PRTrans__Cancel_Reason;
    }

    /**
     *
     * @param pRTransCancelReason
     * The PRTrans__Cancel_Reason
     */
    public void setPRTransCancelReason(String pRTransCancelReason) {
        this.PRTrans__Cancel_Reason = pRTransCancelReason;
    }

    /**
     *
     * @return
     * The lmUsr
     */
    public String getLmUsr() {
        return Lm_Usr;
    }

    /**
     *
     * @param lmUsr
     * The Lm_Usr
     */
    public void setLmUsr(String lmUsr) {
        this.Lm_Usr = lmUsr;
    }

    /**
     *
     * @return
     * The lmDt
     */
    public Date getLmDt() {
        return Lm_Dt;
    }

    /**
     *
     * @param lmDt
     * The Lm_Dt
     */
    public void setLmDt(Date lmDt) {
        this.Lm_Dt = lmDt;
    }

    /**
     *
     * @return
     * The patSign
     */
    public String getPatSign() {
        return Pat_Sign;
    }

    /**
     *
     * @param patSign
     * The Pat_Sign
     */
    public void setPatSign(String patSign) {
        this.Pat_Sign = patSign;
    }

    /**
     *
     * @return
     * The pRSign
     */
    public String getPRSign() {
        return PR_Sign;
    }

    /**
     *
     * @param pRSign
     * The PR_Sign
     */
    public void setPRSign(String pRSign) {
        this.PR_Sign = pRSign;
    }

    /**
     *
     * @return
     * The mode
     */
    public String getMode() {
        return Mode;
    }

    /**
     *
     * @param mode
     * The Mode
     */
    public void setMode(String mode) {
        this.Mode = mode;
    }

    /**
     *
     * @return
     * The pRTransDVOList
     */
    public List<ProcedureSelectedItemsList> getPRTransDVOList() {
        return PRTransDVOList;
    }

    /**
     *
     * @param pRTransDVOList
     * The PRTransDVOList
     */
    public void setPRTransDVOList(List<ProcedureSelectedItemsList> pRTransDVOList) {
        this.PRTransDVOList = pRTransDVOList;
    }

}
