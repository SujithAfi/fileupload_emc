package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 11/8/2016.
 */

public class ProcedureList {
    @SerializedName("CP_Code")
    @Expose
    private String cPCode;
    @SerializedName("Cp_Name")
    @Expose
    private String cpName;

    /**
     *
     * @return
     * The cPCode
     */
    public String getCpCode() {
        return cPCode;
    }

    /**
     *
     * @param cPCode
     * The CP_Code
     */
    public void setCpCode(String cPCode) {
        this.cPCode = cPCode;
    }

    /**
     *
     * @return
     * The cpName
     */
    public String getCpName() {
        return cpName;
    }

    /**
     *
     * @param cpName
     * The Cp_Name
     */
    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

}
