package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 06/06/16.
 */
public class FileType {

    @SerializedName("DocType_Cd")
    @Expose
    private String docTypeCd;
    @SerializedName("DocType_Name")
    @Expose
    private String docTypeName;
    @SerializedName("IsActive")
    @Expose
    private String isActive;

    /**
     *
     * @return
     * The docTypeCd
     */
    public String getDocTypeCd() {
        return docTypeCd;
    }

    /**
     *
     * @param docTypeCd
     * The DocType_Cd
     */
    public void setDocTypeCd(String docTypeCd) {
        this.docTypeCd = docTypeCd;
    }

    /**
     *
     * @return
     * The docTypeName
     */
    public String getDocTypeName() {
        return docTypeName;
    }

    /**
     *
     * @param docTypeName
     * The DocType_Name
     */
    public void setDocTypeName(String docTypeName) {
        this.docTypeName = docTypeName;
    }

    /**
     *
     * @return
     * The isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

}
