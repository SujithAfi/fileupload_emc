package com.afiinfotech.fileuploader.Model;

/**
 * Created by QDES Infotech on 11/5/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LoginModel {

    @SerializedName("Cons_Name")
    @Expose
    private String consName;
    @SerializedName("Cons_Id")
    @Expose
    private String consId;
    @SerializedName("Cons_Istechnician")
    @Expose
    private Integer consIstechnician;
    @SerializedName("Usr_Name")
    @Expose
    private String usrName;
    @SerializedName("Usr_Type")
    @Expose
    private Integer usrType;
    @SerializedName("authenticated")
    @Expose
    private Boolean authenticated;
    @SerializedName("fileTypes")
    @Expose
    private List<FileType> fileTypes = new ArrayList<FileType>();
    /**
     *
     * @return
     * The consName
     */
    public String getConsName() {
        return consName;
    }

    /**
     *
     * @param consName
     * The Cons_Name
     */
    public void setConsName(String consName) {
        this.consName = consName;
    }

    /**
     *
     * @return
     * The consId
     */
    public String getConsId() {
        return consId;
    }

    /**
     *
     * @param consId
     * The Cons_Id
     */
    public void setConsId(String consId) {
        this.consId = consId;
    }

    /**
     *
     * @return
     * The consIstechnician
     */
    public Integer getConsIstechnician() {
        return consIstechnician;
    }

    /**
     *
     * @param consIstechnician
     * The Cons_Istechnician
     */
    public void setConsIstechnician(Integer consIstechnician) {
        this.consIstechnician = consIstechnician;
    }

    /**
     *
     * @return
     * The usrName
     */
    public String getUsrName() {
        return usrName;
    }

    /**
     *
     * @param usrName
     * The Usr_Name
     */
    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    /**
     *
     * @return
     * The usrType
     */
    public Integer getUsrType() {
        return usrType;
    }

    /**
     *
     * @param usrType
     * The Usr_Type
     */
    public void setUsrType(Integer usrType) {
        this.usrType = usrType;
    }

    /**
     *
     * @return
     * The authenticated
     */
    public Boolean getAuthenticated() {
        return authenticated;
    }

    /**
     *
     * @param authenticated
     * The authenticated
     */
    public void setAuthenticated(Boolean authenticated) {
        this.authenticated = authenticated;
    }

    /**
     *
     * @return
     * The fileTypes
     */
    public List<FileType> getFileTypes() {
        return fileTypes;
    }

    /**
     *
     * @param fileTypes
     * The fileTypes
     */
    public void setFileTypes(List<FileType> fileTypes) {
        this.fileTypes = fileTypes;
    }
}
