package com.afiinfotech.fileuploader.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by QDES Infotech on 11/8/2016.
 */

public class SearchResult {

    @SerializedName("Usr_Id")
    @Expose
    private String usrId;
    @SerializedName("PAT_ID")
    @Expose
    private Integer pATID;
    @SerializedName("PAT_NAME")
    @Expose
    private String pATNAME;
    @SerializedName("PAT_SNAME")
    @Expose
    private String pATSNAME;
    @SerializedName("PAT_SEX")
    @Expose
    private String pATSEX;
    @SerializedName("Loc_Id")
    @Expose
    private Integer locId;

    /**
     *
     * @return
     * The usrId
     */
    public String getUsrId() {
        return usrId;
    }

    /**
     *
     * @param usrId
     * The Usr_Id
     */
    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    /**
     *
     * @return
     * The pATID
     */
    public Integer getPatId() {
        return pATID;
    }

    /**
     *
     * @param pATID
     * The PAT_ID
     */
    public void setPatId(Integer pATID) {
        this.pATID = pATID;
    }

    /**
     *
     * @return
     * The pATNAME
     */
    public String getPatName() {
        return pATNAME;
    }

    /**
     *
     * @param pATNAME
     * The PAT_NAME
     */
    public void setPatName(String pATNAME) {
        this.pATNAME = pATNAME;
    }

    /**
     *
     * @return
     * The pATSNAME
     */
    public String getPatSurName() {
        return pATSNAME;
    }

    /**
     *
     * @param pATSNAME
     * The PAT_SNAME
     */
    public void setPatSurName(String pATSNAME) {
        this.pATSNAME = pATSNAME;
    }

    /**
     *
     * @return
     * The pATSEX
     */
    public String getPatSex() {
        return pATSEX;
    }

    /**
     *
     * @param pATSEX
     * The PAT_SEX
     */
    public void setPatSex(String pATSEX) {
        this.pATSEX = pATSEX;
    }

    /**
     *
     * @return
     * The locId
     */
    public Integer getLocId() {
        return locId;
    }

    /**
     *
     * @param locId
     * The Loc_Id
     */
    public void setLocId(Integer locId) {
        this.locId = locId;
    }

}
