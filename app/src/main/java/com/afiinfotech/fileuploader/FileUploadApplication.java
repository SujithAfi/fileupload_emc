package com.afiinfotech.fileuploader;

import android.app.Application;

import com.afiinfotech.fileuploader.Utilis.PostCaptureListener;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by afi-mac-001 on 13/07/16.
 */
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "sujith@afiinfotech.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast)
public class FileUploadApplication extends Application {

    public PostCaptureListener getOnCaptureComplete() {
        return onCaptureComplete;
    }

    public void setOnCaptureComplete(PostCaptureListener onCaptureComplete) {
        this.onCaptureComplete = onCaptureComplete;
    }

    private PostCaptureListener onCaptureComplete;

    public int getViewPageAfterCloseCamera() {
        return viewPageAfterCloseCamera;
    }

    public void setViewPageAfterCloseCamera(int viewPageAfterCloseCamera) {
        this.viewPageAfterCloseCamera = viewPageAfterCloseCamera;
    }

    private int viewPageAfterCloseCamera;
    public int getCurrentViewPage() {
        return currentViewPage;
    }

    public void setCurrentViewPage(int currentViewPage) {
        this.currentViewPage = currentViewPage;
    }

    private int currentViewPage;

    public String getPictureImagePath() {
        return pictureImagePath;
    }

    public void setPictureImagePath(String pictureImagePath) {
        this.pictureImagePath = pictureImagePath;
    }

    private String pictureImagePath;
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
    }

    public PostCaptureListener getOnCapturCompleteListener() {
        return this.onCaptureComplete;
    }

    public void setOnCaptureCompleteListener(PostCaptureListener onCaptureComplete) {
        this.onCaptureComplete = onCaptureComplete;
    }
}
